"""
    Pydantic Models for the Wilax project
"""

import ipaddress
from datetime import datetime
from ipaddress import ip_address
from typing import Optional

from bson import ObjectId
from pydantic import BaseModel, field_validator
from typing_extensions import Annotated

# ObjectId = Annotated[str, AfterValidator(check_object_id)]


def check_ip(value: str) -> bool:
    """
    Validates the str is a valid ip v4 address
    """
    try:
        ip = ip_address(value)
        return_value = True
    except ValueError:
        return_value = False
    return return_value


class APIKeyModel(BaseModel):
    """
    Model for our API Keys
    """

    key: str
    name: str


class MinionModel(BaseModel):
    """
    Model for a Minion
    """

    timestamp: Optional[datetime] = None
    name: str
    version: str
    ip: Optional[str] = None

    @field_validator("ip")
    @classmethod
    def _check_ip(cls, value: str) -> str:
        if check_ip(value) is False:
            raise ValueError("ip has to be a valid ip address")
        return value


class ClientApiKey(BaseModel):
    """
    Model for Client API Keys
    """

    class Config:
        """
        Allow all types
        """

        arbitrary_types_allowed = True

    id: ObjectId
    apikey: str


class Client(BaseModel):
    """
    Model for a Client
    """

    class Config:
        """
        Allow all types
        """

        arbitrary_types_allowed = True

    id: ObjectId
    name: str
