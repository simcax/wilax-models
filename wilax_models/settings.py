"""
    Pydantic Settings for the Wilax project
"""

import os

from pydantic_settings import BaseSettings, SettingsConfigDict


class AuthSettings(BaseSettings):
    """
    Model for Authentication
    """

    auth_type: str = "wilax"
    type: str = ""
    server_pub_path: str = ""
    server_key_path: str = ""
    client_pub_path: str = ""
    client_key_path: str = ""

    model_config = SettingsConfigDict(env_file=f".env")
