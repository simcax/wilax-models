"""Models relating to Wilax API"""

from pydantic import BaseModel


class Token(BaseModel):
    """Class for creating a base token key"""

    api_key: str
    jwt_secret: str


class Keycloak(BaseModel):
    """Class for creating a base keycloak object"""

    server_url: str
    realm_name: str
    client_id: str
    client_secret: str


class Authentication(BaseModel):
    """Class for creating a base authentication object"""

    token: Token
    oauth: Keycloak


class Server(BaseModel):
    """Class for creating a base server object"""

    port: int
    hostname: str


class Config(BaseModel):
    """Class for creating a base config object for the config yaml file"""

    auth_type: str
    authentication: Authentication
    server: Server
