"""
    Test database models for MongoDB
"""
from ipaddress import IPv4Address

import pytest
from bson import ObjectId
from testcontainers.mongodb import MongoDbContainer

from wilax_models.models import (APIKeyModel, Client, ClientApiKey,
                                 MinionModel, check_ip)


def test_ip_address():
    """
    Test it is possible to validate an ip address
    """
    address = "127.0.0.1"
    assert check_ip(address)


def test_ip_address_failing():
    """
    Test a false when checking a wrong ip v4 address
    """
    address = "127.2222.1.1"
    assert check_ip(address) is False


def test_minion_model():
    """
    Test to create a pydantic model for a minion entry without an ip
    """
    minion = MinionModel(name="FirstMinion", version="0.0.1")
    assert isinstance(minion, MinionModel)


def test_minion_model_with_ip():
    """
    Test to create a pydantic model for a minion entry WITH an ip
    """
    minion = MinionModel(name="FirstMinion", version="0.0.1", ip="127.0.0.1")
    assert isinstance(minion, MinionModel)


def test_minion_model_with_wrong_ip():
    """
    Test to create a pydantic model for a minion entry WITH an ip
    """
    with pytest.raises(ValueError):
        minion = MinionModel(name="FirstMinion", version="0.0.1", ip="127.2222.0.1")


def test_client_apikey_model():
    """
    Test to create a pydantic model for an apikey entry
    """
    minion = MinionModel(version="0.0.1", ip="127.0.0.1", name="SomeName")
    with MongoDbContainer("mongo:7.0.2").with_exposed_ports(27017) as mongo:
        db = mongo.get_connection_client().test

        # Let's add a document entry
        insert_id = db.minions.insert_one(minion.model_dump()).inserted_id
    client = Client(id=insert_id, name=minion.name)

    apikey_value = "34567SDFHGert323sdf5"
    apikey = ClientApiKey(apikey=apikey_value, id=client.id)
    assert isinstance(apikey, ClientApiKey)
    assert apikey.apikey == apikey_value


def test_client_model():
    """
    Test a client model can be created
    """
    minion = MinionModel(version="0.0.1", ip="127.0.0.1", name="SomeName")
    with MongoDbContainer("mongo:7.0.2").with_bind_ports(27017, 27017) as mongo:
        db = mongo.get_connection_client().test

        # Let's add a document entry
        insert_id = db.minions.insert_one(minion.model_dump()).inserted_id
    client = Client(id=insert_id, name=minion.name)
    assert isinstance(client.id, ObjectId)
