from wilax_models import models


def test_minion_base_type():
    """
    Tests the base model for a minion at creation time
    so without timestamp and ipaddress
    """
    minion_name = "SomeMinion"
    minion_version = "1.0.0"
    minion = models.MinionModel(name=minion_name, version=minion_version)
    assert minion.ip is None
    assert minion.timestamp is None
    assert minion.name == minion_name
    assert minion.version == minion_version
