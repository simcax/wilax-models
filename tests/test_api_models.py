"""Testing the model for the API models"""

import pytest

from wilax_models.api_models import Authentication, Config, Keycloak, Server, Token


# Test the Token model
def test_token_model():
    token = Token(api_key="", jwt_secret="")
    assert isinstance(token, Token)


def test_token_model_values_are_empty():
    """Testing the token model contains the right values"""
    token = Token(api_key="", jwt_secret="")
    assert token.api_key == ""
    assert token.jwt_secret == ""


def test_token_model_values_are_not_empty():
    """Testing the token model when values are passed"""
    token = Token(api_key="somekey", jwt_secret="somejwtsecret")
    assert "somekey" in token.api_key
    assert "somejwtsecret" in token.jwt_secret


def test_keycloak_model():
    """Testing the keycloak model"""
    keycloak = Keycloak(server_url="", realm_name="", client_id="", client_secret="")
    assert isinstance(keycloak, Keycloak)


def test_keycloak_model_values_are_empty():
    """Testing the keycloak model contains the right values"""
    keycloak = Keycloak(server_url="", realm_name="", client_id="", client_secret="")
    assert keycloak.server_url == ""
    assert keycloak.realm_name == ""
    assert keycloak.client_id == ""
    assert keycloak.client_secret == ""


def test_keycloak_model_values_are_not_empty():
    """Testing the keycloak model when values are passed"""
    keycloak = Keycloak(
        server_url="http://localhost:8080",
        realm_name="wilax",
        client_id="wilax",
        client_secret="someclientsecret",
    )
    assert "http://localhost:8080" in keycloak.server_url
    assert "wilax" in keycloak.realm_name
    assert "wilax" in keycloak.client_id
    assert "someclientsecret" in keycloak.client_secret


def test_authentication_model():
    """Testing the authentication model"""
    authentication = Authentication(
        token=Token(api_key="", jwt_secret=""),
        oauth=Keycloak(server_url="", realm_name="", client_id="", client_secret=""),
    )
    assert isinstance(authentication, Authentication)


def test_authentication_model_values_are_empty():
    """Testing the authentication model contains the right values"""
    authentication = Authentication(
        token=Token(api_key="", jwt_secret=""),
        oauth=Keycloak(server_url="", realm_name="", client_id="", client_secret=""),
    )
    assert authentication.token.api_key == ""
    assert authentication.token.jwt_secret == ""
    assert authentication.oauth.server_url == ""
    assert authentication.oauth.realm_name == ""
    assert authentication.oauth.client_id == ""
    assert authentication.oauth.client_secret == ""


def test_authentication_model_values_are_not_empty():
    """Testing the authentication model when values are passed"""
    authentication = Authentication(
        token=Token(api_key="somekey", jwt_secret="somejwtsecret"),
        oauth=Keycloak(
            server_url="http://localhost:8080",
            realm_name="wilax",
            client_id="wilax",
            client_secret="someclientsecret",
        ),
    )
    assert "somekey" in authentication.token.api_key
    assert "somejwtsecret" in authentication.token.jwt_secret
    assert "http://localhost:8080" in authentication.oauth.server_url
    assert "wilax" in authentication.oauth.realm_name
    assert "wilax" in authentication.oauth.client_id
    assert "someclientsecret" in authentication.oauth.client_secret


def test_server_model():
    """Testing the server model"""
    server = Server(port=8080, hostname="localhost")
    assert isinstance(server, Server)


# test the server model with empty values
def test_server_model_values_are_empty():
    """Testing the server model contains the right values"""
    server = Server(port=8080, hostname="localhost")
    assert server.port == 8080
    assert server.hostname == "localhost"


# Test the server model with values
def test_server_model_values_are_not_empty():
    """Testing the server model when values are passed"""
    server = Server(port=8080, hostname="localhost")
    assert server.port == 8080
    assert server.hostname == "localhost"


# test the config model
def test_config_model():
    """Testing the config model"""
    config = Config(
        auth_type="",
        authentication=Authentication(
            token=Token(api_key="", jwt_secret=""),
            oauth=Keycloak(
                server_url="", realm_name="", client_id="", client_secret=""
            ),
        ),
        server=Server(port=8080, hostname="localhost"),
    )
    assert isinstance(config, Config)


# test the config model with empty values
def test_config_model_values_are_empty():
    """Testing the config model contains the right values"""
    config = Config(
        auth_type="",
        authentication=Authentication(
            token=Token(api_key="", jwt_secret=""),
            oauth=Keycloak(
                server_url="", realm_name="", client_id="", client_secret=""
            ),
        ),
        server=Server(port=8080, hostname="localhost"),
    )
    assert config.auth_type == ""
    assert config.authentication.token.api_key == ""
    assert config.authentication.token.jwt_secret == ""
    assert config.authentication.oauth.server_url == ""
    assert config.authentication.oauth.realm_name == ""
    assert config.authentication.oauth.client_id == ""
    assert config.authentication.oauth.client_secret == ""
    assert config.server.port == 8080
    assert config.server.hostname == "localhost"


# test the config model with values
def test_config_model_values_are_not_empty():
    """Testing the config model when values are passed"""
    config = Config(
        auth_type="",
        authentication=Authentication(
            token=Token(api_key="somekey", jwt_secret="somejwtsecret"),
            oauth=Keycloak(
                server_url="http://localhost:8080",
                realm_name="wilax",
                client_id="wilax",
                client_secret="someclientsecret",
            ),
        ),
        server=Server(port=8080, hostname="localhost"),
    )
    assert config.auth_type == ""
    assert "somekey" in config.authentication.token.api_key
    assert "somejwtsecret" in config.authentication.token.jwt_secret
    assert "http://localhost:8080" in config.authentication.oauth.server_url
    assert "wilax" in config.authentication.oauth.realm_name
    assert "wilax" in config.authentication.oauth.client_id
    assert "someclientsecret" in config.authentication.oauth.client_secret
    assert config.server.port == 8080
    assert config.server.hostname == "localhost"
